from flask import Flask

with open("secret-number.txt") as f:
    secret_number_value = int(f.read())

app = Flask(__name__)


@app.get("/return_secret_number")
def secret_number():
    return {"secret_number": secret_number_value}
